const express = require("express")
const AWS = require("aws-sdk")
const serverless = require("serverless-http")
const Redis = require("ioredis")

const app = express()

app.use(express.json())

AWS.config.update({
  region: "us-east-1", // e.g., 'us-east-1'
  accessKeyId: "AKIAYOZ6MWEDVWDVZANV",
  secretAccessKey: "+/JL/cafiUYPYVVAPlo1hPZ2jcmCE2HVNtx9JYoI",
})

const sqs = new AWS.SQS({ apiVersion: "2012-11-05" })

app.get("/list", async (req, res) => {
  const queueUrl = "https://sqs.us-east-1.amazonaws.com/581562052871/demo-queue"

  try {
    const data = await sqs
      .receiveMessage({
        QueueUrl: queueUrl,
        MaxNumberOfMessages: 9,
        WaitTimeSeconds: 20,
      })
      .promise()

    console.log("response from sqs: ", data)

    count = 0

    for (const msg of data.Messages) {
      const single = msg.Body
      console.log(`${count++} msg: `, single)
    }

    if (data.Messages) {
      return res.status(200).json(data.Messages)
    }

    return res.status(200).json({ message: "No messages in the queue" })
  } catch (error) {
    console.error("Error:", error)
    return res.status(500).json({ message: "Error listing messages" })
  }
})

app.get("/list-all", async (req, res) => {
  const queueUrl = "https://sqs.us-east-1.amazonaws.com/581562052871/demo-queue"
  const maxMessages = 9 // Number of messages to fetch per request

  try {
    let allMessages = []

    // Continuously fetch messages until there are no more left
    let shouldFetch = true
    while (shouldFetch) {
      const data = await sqs
        .receiveMessage({
          QueueUrl: queueUrl,
          MaxNumberOfMessages: maxMessages,
          WaitTimeSeconds: 20,
        })
        .promise()

      if (data.Messages && data.Messages.length > 0) {
        const messages = data.Messages.map((msg, index) => ({
          messageIndex: index + 1,
          body: msg.Body,
          receiptHandle: msg.ReceiptHandle, // You can use this for message deletion
        }))

        allMessages = allMessages.concat(messages)

        // If there are no more messages to fetch, exit the loop
        shouldFetch = data.Messages.length === maxMessages
      } else {
        // If no more messages are returned, exit the loop
        shouldFetch = false
      }
    }

    if (allMessages.length > 0) {
      return res.status(200).json(allMessages)
    } else {
      return res.status(200).json({ message: "No messages in the queue" })
    }
  } catch (error) {
    console.error("Error:", error)
    return res.status(500).json({ message: "Error listing messages" })
  }
})

app.get("/redis-ec2", async (req, res) => {
  const redis = new Redis({
    host: "35.175.231.104",
    port: 6379,
  })

  await redis.set("name", "shubham")
  await redis.set("age", "22")
  return res.status(200).json({
    message: "Message processed and sent to Redis.",
  })
})

app.get("/list-redis", async (req, res) => {
  const queueUrl = "https://sqs.us-east-1.amazonaws.com/581562052871/demo-queue"

  try {
    const data = await sqs
      .receiveMessage({
        QueueUrl: queueUrl,
        MaxNumberOfMessages: 5,
        WaitTimeSeconds: 20,
      })
      .promise()

    const redis = new Redis({
      host: "ankit-cluster.x9esla.ng.0001.use1.cache.amazonaws.com",
      port: 6379,
    })

    await redis.set("users", data.Messages)

    return res.status(200).json({
      message: "Users fetched and saved to Redis ec2.",
      data: data.Messages,
    })
  } catch (error) {
    console.error("Error:", error)
    return res.status(500).json({ message: "Error listing messages" })
  }
})

app.get("/save-users-redis", async (req, res) => {
  const queueUrl =
    "https://sqs.us-east-1.amazonaws.com/581562052871/legacy-queue"

  try {
    const data = await sqs
      .receiveMessage({
        QueueUrl: queueUrl,
        MaxNumberOfMessages: 5,
        WaitTimeSeconds: 20,
      })
      .promise()

    const redis = new Redis({
      host: "ankit-cluster.x9esla.ng.0001.use1.cache.amazonaws.com",
      port: 6379,
    })

    await redis.set("event", data.Messages[0].Body)

    return res.status(200).json({
      message: "Users fetched and saved to Redis ec2.",
      data: data.Messages,
    })
  } catch (error) {
    console.error("Error:", error)
    return res.status(500).json({ message: "Error listing messages" })
  }
})

app.get("/hello", (req, res) => {
  return res.status(200).json({
    message: "Hello and Welcome to connection with SQS and Redis ElastiCache",
  })
})

app.get("/welcome", (req, res) => {
  return res.status(200).json({
    message: "welcome",
  })
})

// module.exports.handler = serverless(app);

app.listen(8000, () => {
  console.log("Server started on 8000")
})
